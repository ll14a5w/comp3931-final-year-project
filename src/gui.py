
import sys
from PyQt6.QtWidgets import QDialog, QApplication, QPushButton, QVBoxLayout, QLabel, QComboBox, QLineEdit, QCheckBox, QFileDialog, QMainWindow, QWidget, QGridLayout
import pandas as pd
from PyQt6.QtCore import Qt
from PyQt6.QtGui import QAction, QIntValidator
from matplotlib.backends.backend_qt5agg import FigureCanvasQTAgg as FigureCanvas
from matplotlib.backends.backend_qt5agg import NavigationToolbar2QT as NavigationToolbar
import matplotlib.pyplot as plt
import enums
from matplotlib import cm
import data_processor as dp
import missingno as msno
from upsetplot import UpSet, from_indicators, plot

class Base(QMainWindow):
    # constructor
    def __init__(self):
        # Initial Window where the graph/visualisation will show
        super().__init__()
        self.setFixedSize(1366, 768)
        self.main = QWidget(self)
        self.setCentralWidget(self.main)
        # Initialise the menubar
        self.buttons = []
        # self.error will handle the error. Only 1 error will happen at a time so it can be frequently adjusted
        self.error = None
        self.main_menu = self.menuBar()
        file_menu = self.main_menu.addMenu("File")
        open_file = QAction("Load Data", self)
        open_file.setShortcut('Ctrl+O')
        open_file.triggered.connect(self.set_csv)
        file_menu.addAction(open_file)
        self.figure = plt.figure(figsize=(18,6),dpi=100)
        # self.figure.tight_layout()
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, self)
        # Initialise all the buttons
        plot_button = QPushButton("Plot", self)
        plot_button.pressed.connect(self.plot_box)
        range_button = QPushButton("Select Ranges/Subsets", self)
        range_button.pressed.connect(self.set_ranges)
        heatmap_button = QPushButton("Show Randomness Heatmap", self)
        heatmap_button.pressed.connect(self.missing_no)
        ax = self.figure.add_subplot(111)
        # Append the buttons for easy activation later
        self.append_buttons(plot_button, range_button, heatmap_button)
        self.grid = QGridLayout()
        # Initialise range as none for if there is no range set by the RangeUI
        self.range = None
        self.data_loaded = QLabel()
        self.range_loaded = QLabel()
        self.grid.addWidget(self.data_loaded, 4,0,1,5)
        # Setup the grid
        # This is ugly.
        # Perhaps modulisation would make it look better.
        self.grid.addWidget(self.toolbar, 1,0,1,5)
        self.grid.addWidget(self.canvas, 2, 0, 1, 5)
        self.grid.addWidget(plot_button, 3, 0, 1, 5)
        self.grid.addWidget(range_button, 0, 0, 1, 2)
        self.grid.addWidget(heatmap_button, 0, 2, 1, 2)
        self.grid.addWidget(self.range_loaded,0, 4, 1, 1)
        # Set the grid's layout
        self.main.setLayout(self.grid)

    def set_csv(self):
        # Get the CSV filepath and save it, retrieve headers
        self.filepath = QFileDialog.getOpenFileName(self, 'Open File', filter="CSV Files (*.csv)")[0]
        if self.filepath == '':
            return
        else:
            data_processor = dp.DataProcessor(self.filepath)
            headers = data_processor.get_file_headers()
            # Send the headers to the processor GUI
            self.processorGUI = DataGUI(headers, data_processor, self)
            self.processorGUI.show()


    def append_buttons(self, *buttons):
        # Set the group of buttons for quick adjustments in adjust_interactives()
        for button in buttons:
            self.buttons.append(button)
            print(self.buttons)
        self.adjust_interactives('disable')

    def adjust_interactives(self, state):
        # Set the buttons to enable/disable
        if state == 'enable':
            for button in self.buttons:
                button.setEnabled(True)
            # Add a label at the bottom for
            self.data_loaded.setText("{} loaded. {} rows and {} columns parsed.".format(
            self.filepath, self.data.shape[0], self.data.shape[1]))
            self.main.setLayout(self.grid)
        elif state == 'disable':
            for button in self.buttons:
                button.setEnabled(False)

    def set_range_loaded(self):
        self.range_loaded.setText("Observing row range: {}".format(
            self.range
        ))
        self.range_loaded.setAlignment(Qt.AlignmentFlag.AlignCenter)

    def missing_no(self):
        if self.range is None:
            # If there is no range set, use the full dataset.
            # Magma is used as it is colourblind friendly. Sadly the colourmaps couldn't be standardised.
            plot = msno.heatmap(self.data, cmap='magma')
            figure = plot.get_figure()
            plt.suptitle("""
            Missing Data Heatmap Matrix.""")
            self.heatmapGUI = HeatmapGUI(figure)
            self.heatmapGUI.show()
        else:
            lower, upper = self.range.split(":")
            # If there is a range, use only that range
            plot = msno.heatmap(self.data.iloc[int(lower):int(upper),:], cmap='magma')
            figure = plot.get_figure()
            plt.suptitle("""
            Missing Data Heatmap Matrix.""")
            self.heatmapGUI = HeatmapGUI(figure)
            self.heatmapGUI.show()


    def set_ranges(self):
        self.range_ui = RangeGUI(self.data.shape[0], self)
        self.range_ui.show()
        self.get_range()

    def get_range(self):
        print(self.range)
    
    def plot_canvas(self, title, headers):
        # Make sure data is there
        if self.processorGUI.data_processor.missing_data is None:
            data = self.processorGUI.data_processor.get_missing()
        else:
            data = self.processorGUI.data_processor.missing_data
        if self.range is None:
            self.figure.clear()
            for column in headers:
                data[column] = data[column].astype('bool')
            data = data.set_index(headers)
            print(data.index)

            # Stacked bar chart, modified from upsetplot's site to use 'missing' variable.
            upset = UpSet(data,
                intersection_plot_elements=0, element_size=None)
            upset.add_stacked_bars(by="Missing", colors=cm.Accent,
                        title="Count By Missing")
            upset.plot(fig=self.figure)
            if title:
                plt.suptitle(title)
            self.canvas.draw_idle()
        else:
            # If there is a range to look over, then it will show only that range.
            lower, upper = self.range.split(":")
            self.figure.clear()
            for column in headers:
                data[column] = data[column].astype('bool')
            data = data.set_index(headers)
            upset = UpSet(data.iloc[int(lower):int(upper),:],
                intersection_plot_elements=0, element_size=None)
            # Element_size = None makes it fit the figure better
            upset.add_stacked_bars(by="Missing", colors=cm.Accent,
                        title="Count By Missing")
            upset.plot(fig=self.figure)
            if title:
                plt.suptitle(title)
            self.canvas.draw_idle()

    def plot_box(self):
        self.new = PlotBoxGUI(self, self.data)
        self.new.show()
            # new = elf, parent, data
class DataGUI(QWidget):
    # In an ideal world there'd be a few widget boxes given the small # of possible windows.
    def __init__(self, headers, data_processor, parent):
        super().__init__()
        self.setWindowTitle("Header Adjustment Window")
        layout = QGridLayout()
        # Set the backend helper
        self.data_processor = data_processor
        self.parent = parent
        # Combobox/Label for easy access
        self.comboboxes = []
        self.labels = []
        label = QLabel("Please select the index for your data, and remove any unnecessary fields from your data.")
        layout.addWidget(label,0,0)
        # Go through each header and generate a label and a combobox for each header.
        for i,header in enumerate(headers):
            label = QLabel(header)
            combobox = QComboBox()
            self.comboboxes.append(combobox)
            self.labels.append(label)
            layout.addWidget(label, i+1, 0)
            # enums keys are remove, index or nothing. bool overrides NaNs at this stage
            combobox.addItems(list(enums.pandas_type_dict.keys()))
            layout.addWidget(combobox, i+1, 1)
        confirm_button = QPushButton("Confirm Index")
        confirm_button.pressed.connect(self.confirm)
        # +2 because 0 is occupied and we we dont want to override a row
        layout.addWidget(confirm_button, len(self.labels)+2,0)
        self.setLayout(layout)

    def confirm(self):
        # Send the results to the backend helper once more then close
        print(self.labels)
        self.data_processor.set_header_configuration(self.labels,self.comboboxes)
        self.parent.data = self.data_processor.data
        self.parent.adjust_interactives('enable')
        self.close()

class PlotBoxGUI(QWidget):
    def __init__(self, parent, data):
        # Setup the plot box. 
        # It is quite self explanitory; initialise all that is important 
        # and allow users to modify the plot with the results
        super().__init__()
        self.setWindowTitle("Plot")
        self.parent = parent
        self.grid = QGridLayout()
        self.plot_title = QLabel("What would you like to name your plot?")
        self.plot_title_editor = QLineEdit()
        self.index_list = []
        self.list_of_headers = data.columns.tolist()
        column_no = len(self.list_of_headers)
        self.grid.addWidget(self.plot_title, 0, 0, 1, column_no)
        self.grid.addWidget(self.plot_title_editor, 1, 0, 1, column_no)
        # for loop allows for less bulky code
        for i, header in enumerate(self.list_of_headers):
            checkbox = QCheckBox(header,self)
            self.grid.addWidget(checkbox, 2, i, 1, 1)
            self.index_list.append(checkbox)
        check_button = QPushButton("Plot!", self)
        check_button.pressed.connect(self.get_indices)
        self.grid.addWidget(check_button,3, 0, 1, column_no)
        self.setLayout(self.grid)

    def get_indices(self):
        # Get the headers to be used as indices 
        index_data = []
        for index in self.index_list:
            if index.isChecked():
                index_data.append(index.text())
        self.plot(index_data)

    def plot(self, indices):
        # There needs to be more than 1 index for the upset plot.
        if len(indices) < 2:
            self.error = ErrorBox("Please select at least two variables to observe.", "Index Error")
            self.error.show()
            return
        else:
            # Title will be title or ""/nothing.
            title = str(self.plot_title_editor.text())
            self.parent.plot_canvas(title=title, headers=indices)
            self.close()

class RangeGUI(QWidget):
    # If ttime permitted I would alter this code to be less messy and a little cleaner.
    def __init__(self, upper_bound, parent):
        super().__init__()
        self.setWindowTitle("Range Selecter")
        self.grid = QGridLayout()
        self.grid.addWidget(QLabel("""
        Please select the index range you wish to plot below.
        If a box is empty, it will be automatically filled."""),0,0,1,3)
        self.lower = QLineEdit()
        self.lower.setText("0")
        self.upper_bound = upper_bound
        self.lower.setValidator(QIntValidator(0, upper_bound-1))
        self.upper = QLineEdit()
        self.upper.setValidator(QIntValidator(1, upper_bound))
        self.upper.setText(str(upper_bound))
        self.parent = parent
        self.grid.addWidget(self.lower,1,0,1,1)
        self.grid.addWidget(QLabel(":"),1,1,1,1)
        self.grid.addWidget(self.upper,1,2,1,1)
        self.error = QLabel()
        self.grid.addWidget(self.error,2,0,1,3)
        self.range_value = None
        confirm_button = QPushButton("Confirm",self)
        self.grid.addWidget(confirm_button, 3,0,1,3)
        confirm_button.pressed.connect(self.validate)
        confirm_button = QPushButton("Reset",self)
        self.grid.addWidget(confirm_button, 4,0,1,3)
        confirm_button.pressed.connect(self.reset)
        self.setLayout(self.grid)

    def validate(self):
        # Check to ensure the lower/upper bounds are valid
        lower_bound = int(self.lower.text())
        upper_bound = int(self.upper.text())
        if lower_bound > upper_bound:
            self.error.setText("The lower bound cannot be higher than the upper bound.")
        elif upper_bound > self.upper_bound:
            self.error.setText("The upper bound cannot be higher than {}".format(self.upper_bound))
        elif lower_bound < 0:
            self.error.setText("The lower bound cannot be lower than 0.")
        else:
            self.range_value = f'{lower_bound}:{upper_bound}'
            self.return_range()
            self.close()

    def reset(self):
        # Reset the range to 0:max_size
        self.lower.setText("0")
        self.upper.setText(str(self.upper_bound))
        self.range_value = f'0:{self.upper_bound}'
        self.return_range()

    def return_range(self):
        # Send the range back to the main GUI element so heatmap can work.
        # It also sets the loaded range QLabel beside the buttons
        self.parent.range = self.range_value
        self.parent.set_range_loaded()


class HeatmapGUI(QWidget):
    # Simple GUI interface to show a heatmap based on missing_no.
    # No user input required. The toolbar is also a little pointless here but allows for saving.
    def __init__(self, figure):
        super().__init__()
        self.setWindowTitle("Heatmap Displayer")
        self.figure = figure
        self.grid = QVBoxLayout()
        self.figure.tight_layout()
        self.figure.set_size_inches(20,10)
        self.figure.set_dpi(100)
        self.canvas = FigureCanvas(self.figure)
        self.toolbar = NavigationToolbar(self.canvas, self)
        self.grid.addWidget(self.toolbar)
        self.grid.addWidget(self.canvas)
        self.canvas.draw()
        self.setLayout(self.grid)


class ErrorBox(QWidget):
    # Basic Error Box.
    # Show the error type in the title, display the error and allow for closing
    def __init__(self, error : str, title : str):
        super().__init__()
        self.setWindowTitle(title)
        grid = QGridLayout()
        # Show an error label that is determined upon each ErrorBox
        error_label = QLabel(error)
        grid.addWidget(error_label,0,1)
        button = QPushButton("Okay")
        grid.addWidget(button,1,1)
        button.clicked.connect(self.close_window)
        self.setLayout(grid)

    def close_window(self):
        # Close the window for convenience
        self.close()

if __name__ == '__main__':
        # Create application
        app = QApplication(sys.argv)
        # Create Base Window
        main = Base()
        # Show the window
        main.show()
        # Until closed, run app
        sys.exit(app.exec())