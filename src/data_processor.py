import pandas as pd
from pandas.errors import EmptyDataError as EDE
import numpy as np
from gui import ErrorBox
from enums import pandas_type_dict as enum_dict
class DataProcessor:
    def __init__(self, filepath):
        self.filepath = filepath
        self.id = False
        self.headers = []
        self.missing = []
        self.missing_data = None
    def try_read(self, headers=False):
        # Try read the file, returning either the file or headers
        if headers is True:
            try:
                return pd.read_csv(self.filepath,nrows=0).columns.tolist()
            except:
                self.error = ErrorBox("There is an issue reading this CSV file, please try again.", "Error Occured Whilst Reading")
                self.error.show()
        else:
            try:
                # print("Test")
                # print(self.headers)
                return pd.read_csv(self.filepath, usecols=list(self.headers))
            except EDE as e:
                # self.error = ErrorBox("There is an issue reading this CSV file, please try again.", "Error Occured Whilst Reading")
                self.error = ErrorBox(e,e)
                self.error.show()


    def get_file_headers(self):
        return self.try_read(headers=True)

    def get_missing(self, data):
        self.missing_data = data.copy()
        self.missing_data['Missing'] = np.where(data.isna().any(axis=1) == True, True, False)
        return self.missing_data

    def set_header_configuration(self, labels, comboboxes):
        # Go through each label and it's corresponding combobox to determine what should be done in regards to headers
        for i, label in enumerate(labels):
            current_text = label.text()
            current_combobox = comboboxes[i].currentText()
            # Only 1 index
            if current_combobox == 'Index':
                # Set the ID if one isn't there already
                continue
            # Anything with ignore isn't added
            elif current_combobox == 'Remove':
                continue
            else:
                # Any non index / non-ignored header is here
                self.headers.append(current_text)
        print(self.headers)
        self.data = self.try_read(headers=False)
        self.get_missing(self.data)

